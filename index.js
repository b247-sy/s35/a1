const express = require("express");
const mongoose = require("mongoose");

const app = express();
const port = 3005;


app.use(express.json());

app.use(express.urlencoded({extended:true}));

mongoose.connect("mongodb+srv://Jeannysy:Champoi143@zuitt-bootcamp.2sumcro.mongodb.net/?retryWrites=true&w=majority", 
	{
		useNewUrlParser : true,
		useUnifiedTopology : true
	}
);

let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));

db.on("open", () => console.log("We're connected to the cloud database"));

/*Create a User schema.*/

const userSchema = new mongoose.Schema({
	username : String,
	password : String
})


/*Create a User model.*/
const User = mongoose.model("User", userSchema);

/*Create a POST route that will access the /signup route that will create a user.*/

app.post("/signup", (request, response) => {
	
	User.findOne({username: request.body.username}, (error, result) => {
		if(result != null && result.username == request.body.username){
			return response.send('Duplicate user found!')
		} 

		let newUser = new User({
			username: request.body.username,
			password: request.body.password,
		})

		newUser.save((error, savedUser) => {
			if(error){
				return console.error(error)
			} else {
				return response.status(200).send('New User Registered!')
			}
		})
	})
})



app.listen(port, () => console.log(`Server is now running at port ${port}`));
